# README #

This README documents the steps needed to get up and running.

### What is this repository for? ###

* This repository holds all of the content for PORT1 of the CIS5014 Module for 21/22
* Full name: Harvey Thomas Andrew Sullivan
* ID: st20160042

### What can I find in this repository? ###

* In this repository, you should find the following:
* - Assets folder, containing all created assets on their own, except some 3D models
* - Blender Files, containing all Blender 3d .blend and .fbx files
* - Tank Game, containing all Unity content. Subfolder 'Builds' contains playable demos.
* - Progress folder, containing screenshots documenting the work over time
* - PowerPoint Presentation 'CIS5014_PORT1_21-ST20160042' discussed in video
* - Word document 'Reflection of Level Design'
* - Video .mp4 'Presentation Video'

### Who do I talk to? ###

* Harvey Sullivan - st20160042@outlook.cardiffmet.ac.uk
* Or contact on Microsoft Teams