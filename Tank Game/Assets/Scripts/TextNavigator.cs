using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TextNavigator : MonoBehaviour
{

    public TextMeshProUGUI mainTextField;
    public TextMeshProUGUI secondaryTextField;

    private string nextMessage = "Press SPACE to dismiss";
    private string wasdMessage = "press WASD to move";
    private string mouseMovementMessage = "move the mouse around to aim";
    private string changeSceneMessage = "when ready, press TAB to change scene.";



    // Start is called before the first frame update
    void Start()
    {
        secondaryTextField.text = nextMessage;
        mainTextField.text = wasdMessage;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.X))
        {
            mainTextField.text = mouseMovementMessage;
            if (Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.X))
            {
                mainTextField.text = changeSceneMessage;
                if (Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.X))
                {
                    secondaryTextField.text = "go ahead, press TAB";
                }
            }
        }
    }
}
