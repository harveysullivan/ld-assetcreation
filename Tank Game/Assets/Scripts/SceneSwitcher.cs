using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneSwitcher : MonoBehaviour
{
    [SerializeField]
    private int levelIndex;
    
    // Update is called once per frame
    void Update()
    {
        
 

        if (Input.GetKey(KeyCode.Tab) || Input.GetKey(KeyCode.Return))
        {
            // Change to Level 1
            SceneManager.LoadScene(levelIndex);
        }
    }
}
