using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicMovement : MonoBehaviour
{

    public Rigidbody rb;

    private float movementFloat;


    // Turret rotation
    private float turretRotation;

    [SerializeField]
    private float turretMinClamp;

    [SerializeField]
    private float turretMaxClamp;

    [SerializeField]
    private Transform turretTransform;

    // Barrel rotation 
    private float barrelRotation;

    [SerializeField]
    private float barrelMinClamp;

    [SerializeField]
    private float barrelMaxClamp;

    [SerializeField]
    private Transform barrelTransform;

    // Base rotation

    private float sidewaysRotation;

    [SerializeField]
    private float baseRotationSpeed;

    [SerializeField]
    private float movementSpeed;

    [SerializeField]
    private float maxSpeed;



    private void Start()
    {
        
    }


    void Update()
    {
        MoveTank();
        RotateBase();
        RotateTurret();
        RotateBarrel();

    }

    private void FixedUpdate()
    {
        if (Mathf.Abs(movementFloat) != 0)
        {
            rb.AddForce(-transform.forward * movementFloat);
            if (rb.velocity.magnitude > maxSpeed)
                rb.velocity = rb.velocity.normalized * maxSpeed;

        }
    }

    void MoveTank()
    {
        if (Input.GetAxis("Vertical") != 0)
        {
            movementFloat = Input.GetAxis("Vertical") * movementSpeed;
        }
    }

    void RotateTurret()
    {
        turretRotation += Input.GetAxis("Mouse X");
        turretRotation = Mathf.Clamp(turretRotation, turretMinClamp, turretMaxClamp);
        turretTransform.localRotation = Quaternion.AngleAxis(turretRotation, Vector3.up);

    }

    void RotateBase()
    {
        float y = Input.GetAxis("Horizontal");
        // Adjust speed if necessary
        sidewaysRotation += y * baseRotationSpeed * Time.deltaTime;
        transform.rotation = Quaternion.Euler(transform.eulerAngles.x, sidewaysRotation, transform.eulerAngles.z);
    }
    
    void RotateBarrel()
    {
        barrelRotation += Input.GetAxis("Mouse Y");
        barrelRotation = Mathf.Clamp(barrelRotation, barrelMinClamp, barrelMaxClamp);
        barrelTransform.localRotation = Quaternion.AngleAxis(barrelRotation, Vector3.right);
    }

}
